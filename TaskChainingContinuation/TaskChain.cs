﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TaskChainingContinuation
{
    public static class TaskChain
    {
        public static List<int> Task1()
        {
            var array = new List<int>();
            Random random = new Random();
            for (int i = 0; i < 10; i++)
            {
                array.Add(random.Next(0,100));
            }
            string data = "Task1:"+ string.Join(" - ", array);
            Console.WriteLine(data);
            return array;
        }

        public static List<int> Task2(List<int> list)
        {
            Random random = new Random();
            int randomNumber = random.Next(0,10);
            var array =  list.Select(x => x * randomNumber).ToList();
            string data = "Task2:" + string.Join(" - ", array);
            Console.WriteLine(data);
            return array;
        }

        public static List<int> Task3(List<int> list)
        {
            var array = list.OrderBy(x => x).ToList();
            string data = "Task3:" + string.Join(" - ", array);
            Console.WriteLine(data);
            return array;
        }

        public static double Task4(List<int> list)
        {
            double avg = list.Average(x => x);
            Console.WriteLine("Task4:" + avg);
            return avg;
        }
    }
}


using NUnit.Framework;
using System.Threading.Tasks;

namespace TaskChainingContinuation.Test
{
    public static class Tests
    {
        /// <summary>
        /// Task 1: creates an array of 10 random integers.
        /// </summary>
        /// <returns>Test1.</returns>
        [Test]
        public static async Task Test1()
        {
            var task = Task.Run(TaskChain.Task1);
            await task;
            Assert.IsTrue(task.Result.Count > 0);
        }

        /// <summary>
        /// Task 1: creates an array of 10 random integers.
        /// Task 2: multiplies the array by a randomly generated number.
        /// </summary>
        /// <returns></returns>
        [Test]
        public static async Task Test2()
        {
            var task = Task.Run(TaskChain.Task1);

            await

            task.ContinueWith(
                result =>
                {
                    TaskChain.Task2(task.Result);
                }, TaskContinuationOptions.RunContinuationsAsynchronously);
            Assert.IsTrue(task.Result.Count > 0);
        }

        /// <summary>
        /// Task 1: creates an array of 10 random integers.
        /// Task 2: multiplies the array by a randomly generated number.
        /// Task 3: sorts the array by ascending.
        /// </summary>
        /// <returns></returns>
        [Test]
        public static async Task Test3()
        {
            var task = Task.Run(TaskChain.Task1);

            await

            task
            .ContinueWith(
                result =>
                {
                    TaskChain.Task2(task.Result);
                }, TaskContinuationOptions.RunContinuationsAsynchronously)
            .ContinueWith(
                result =>
                {
                    TaskChain.Task3(task.Result);
                }, TaskContinuationOptions.RunContinuationsAsynchronously);
            Assert.IsTrue(task.Result.Count > 0);
        }

        /// <summary>
        /// Task 1: creates an array of 10 random integers.
        /// Task 2: multiplies the array by a randomly generated number.
        /// Task 3: sorts the array by ascending.
        /// Task 4: calculates the average value.
        /// </summary>
        /// <returns></returns>
        [Test]
        public static async Task Test4()
        {
            var task = Task.Run(TaskChain.Task1);

            await

            task
                .ContinueWith(
                result =>
                {
                    TaskChain.Task2(task.Result);
                }, TaskContinuationOptions.RunContinuationsAsynchronously)

                .ContinueWith(
                result =>
                {
                    TaskChain.Task3(task.Result);
                }, TaskContinuationOptions.RunContinuationsAsynchronously)

                .ContinueWith(
                result =>
                {
                    TaskChain.Task4(task.Result);
                }, TaskContinuationOptions.RunContinuationsAsynchronously);

            Assert.IsTrue(task.Result.Count > 0);
        }
    }
}